<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="asset/img/favicon.ico">
	<title>Tibera Hotel</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=EB+Garamond:400,500" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	    crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	    crossorigin="anonymous">
	<link href="<?php bloginfo('template_url'); ?>/asset/fontello/css/marker.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" media="all">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />

	<?php wp_head(); ?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

	<style media="all">
		.flwrap {
			position: absolute;
			top: 50%;
			left: 50%;
			margin: 0 auto;
			z-index: 9;
			transform: translate(-50%,-50%);
		}

		.flex-container {
			display: flex;
			justify-content: center;
		}

		.flex-container > div {
			width: 30vw;
			padding: 10px;
			text-align: center;
			line-height: 75px;
			transition: transform .3s cubic-bezier(.34,2,.6,1),box-shadow .2s ease;
		}

		.flex-container > div:hover {
			-webkit-transform: translateY(-10px);
		}

		.flexslider .slides li {
			transition: transform 4s ease-in-out;
		}

		.flexslider .slides li.flex-active-slide {
			transform: scale(1.2);
		}

		.homepage .section.main-slider:after {
			position: absolute;
			background: url("<?php bloginfo('template_directory'); ?>/asset/img/dot.png");
			display: block;
			content: "";
			top: 0;
			bottom: 0;
			width: 100%;
			z-index: 8;
		}

		.outer-content {
			position: relative;
		}

		.outer-content:before {
			display: block;
			content: "";
			background-color: rgba(0,0,0,.5);
			width: 100%;
			height: 100%;
			position: absolute;
			opacity: .5;
			transition: all ease .5s
		}

		.outer-content:hover::before {
			opacity: 0;
			transition: all ease .5s
		}



		.outer-content .box_content {
			position: absolute;
	    bottom: 0;
	    z-index: 1;
	    background: #9e358b;
	    width: 100%;
		}

		.outer-content .box_content h4 {
			color: #fff;
			margin-bottom: 0;
			padding: 5px 0;
		}

	</style>

</head>

<body>

	<div id="wrapper" class="homepage">

		<div class="section main-slider">
			<?php

			$images = get_field('slider');

			if( $images ): ?>
			    <div id="slider" class="flexslider">
			        <ul class="slides">
			            <?php foreach( $images as $image ): ?>
			                <li style="background-image: url('<?php echo $image['url']; ?>')">
			                </li>
			            <?php endforeach; ?>
			        </ul>
			    </div>
			<?php endif; ?>

			<div class="flwrap">
				<div class="flex-container">
				<?php get_template_part('property'); ?>
				</div>
			</div>

		</div>

	</div><!-- end .wrapper -->

	<script type="text/javascript" charset="utf-8">
	  $(window).load(function() {
	    $('.flexslider').flexslider({
				animation: "fade",
				slideshowSpeed: 6000,
				animationSpeed: 3000,
				controlNav: false,
				pauseOnAction: false,
			});
	  });
	</script>

</body>

</html>
