## 07 January 2019 - v1.0.2 ##
* tidy up

## 15 November 2018 - v1.0.1 ##
* delete unnecessary feature
* styling slider

## 14 November 2018 - v1.0.0 ##
* Initial Release
