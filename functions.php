<?php
    define('karisma_text_domain', get_template());

    if (function_exists('add_theme_support'))
        {
            // Add Menu Support
            add_theme_support('menus');

            // Add Thumbnail Theme Support
            add_theme_support('post-thumbnails');
            add_image_size('large', 700, '', true);
            add_image_size('medium', 320, 200, true);
            add_image_size('small', 120, '', true);
            add_image_size('full');

            // Enables post and comment RSS feed links to head
            add_theme_support('automatic-feed-links');

            add_theme_support('title-tag');
                // Localisation Support
            load_theme_textdomain(karisma_text_domain, get_template_directory_uri() . 'vendor/languages');
        }
    if ( ! function_exists ( 'add_file_types_to_uploads' ) ) {
        function add_file_types_to_uploads($file_types){
            $new_filetypes = array();
            $new_filetypes['svg'] = 'image/svg+xml';
            $file_types = array_merge($file_types, $new_filetypes );
            return $file_types;
        }
    }

    require_once ( get_stylesheet_directory() . '/inc/plugin-update-checker/plugin-update-checker.php' );
        $updateChecker = Puc_v4_Factory::buildUpdateChecker(
            'https://bitbucket.org/hoteldirectdev/hoteldirect-company-landing/',
            __FILE__,
            'hoteldirect-company-landing'
        );

        $updateChecker->setAuthentication( array(
            'consumer_key' => 'suuRULgmY5pFExcvjk',
            'consumer_secret' => 'HQZMzt3xHf2B8ZdfU7dvVtabYskZEaFJ',
        ) );

        $updateChecker->setBranch( 'master' );

    ?>
