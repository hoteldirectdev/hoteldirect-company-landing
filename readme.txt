=== Hoteldirect Company Profile ===
Contributors: Hotel Direct ID
Tags: simple, company profile
Requires at least: 4.8
Stable tag: v1.0.2
Version: 1.0.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Hoteldirect Company Profile


== Installation ==
This section describes how to install the child theme and get it working.

1. You need to install and activate the parent theme first, indohotelswp
2. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Changelog ==

= 1.0.2 =
* tidy up

= 1.0.1 =
* delete unnecessary feature
* styling slider

= 1.0.0 =
* initial release
